const express = require("express");

const app = express();

app.use(express.json())
const port = 4000;

let courses = [
	{

		name: "Python 101",
		description: "Learn Python",
		price: 25000
	},
	{

		name: "ReactJS 101",
		description: "Learn React",
		price: 35000
	},
	{

		name: "ExpressJS 101",
		description: "Learn ExpressJS",
		price: 28000
	},

];

let users = [

	{
		email: "marybell_knight",
		password: "merrymarybell"
	},
	{
		email: "janedoePriest",
		password: "jobPriest100"
	},
	{
		email: "kimTofu",
		password: "dubutofu"
	},

]

// app.listen(port,() => console.log('Express API running at port 4000'))
app.use(express.json())
app.get('/',(req,res)=>{

	res.send('Hello from our first ExpressJS Route');
})

app.post('/',(req,res)=>{

	res.send('Hello from our first ExpressJS Post Method Route');
})

app.put('/',(req,res)=>{

	res.send('Hello from our put method route');
})

app.delete('/',(req,res)=>{

	res.send('Hello from a delete method route');
})

app.get('/courses',(req,res)=>{

	res.send(courses);

})

app.post('/courses',(req,res)=>{

	courses.push(req.body);
	// console.log(req.body);
	res.send(courses);



})
	// ACTIVITY

app.get('/users',(req,res)=>{

	res.send(users);

})

app.post('/users',(req,res)=>{

	users.push(req.body);
	// console.log(req.body);
	res.send(users);

})

app.delete('/users',(req,res)=>{

	users.pop();
	res.send(users);

})
app.listen(port,() => console.log('Express API running at port 4000'))

